#include <stdio.h>

#include "util.h"

void printValue(object input);

int Print(object input)
{
	switch (TYPE(input))
	{
	case errorObject:
		fprintf(stderr, "\nГРЕШКА: %s\n\n", ERR(input));
		break;
	case EOFObject:
		return 0;
		break;
	case unspecifiedObject:
		break;
	default:
		printf("\n");
		printValue(input);
		printf("\n\n");
		break;
	}

	return 1;
}

void printValue(object input)
{
	switch (TYPE(input))
	{
	case nilObject:
		printf("()");
		break;
	case consObject:
		printf("(");
		object *currentCell = &input;
		while (TYPE(*currentCell) == consObject)
		{
			printValue(CAR(*currentCell));
			if (TYPE(CDR(*currentCell)) == consObject)
			{
				printf(" ");
			}
			currentCell = &CDR(*currentCell);
		}
		if (TYPE(*currentCell) != nilObject)
		{
			printf(" . ");
			printValue(*currentCell);
		}
		printf(")");
		break;
	case numberObject:
		if (NUM_TYPE(input) == fractionNum)
		{
			printf("%lld", NUM_NUMER(input));
			if (NUM_DENOM(input) != 1)
			{
				printf("/%lld", NUM_DENOM(input));
			}
		}
		else
		{
			printf("%LF", NUM_REAL(input));
		}
		break;
	case symbolObject:
		printf("%s", SYM(input));
		break;
	case procedureObject:
		printf("<процедура:%s>", PROC_TYPE(input) == builtinProc ?
				"уграђена" : "сложена");
		break;
	case boolObject:
		printf("#%s", BOOL(input) ? "и" : "л");
		break;
	case stringObject:
		printf("\"%s\"", STR(input));
		break;
	case charObject:
		printf("#\\");
		switch (CHR(input))
		{
		case L' ':
			printf("размак");
			break;
		case L'\n':
			printf("новиред");
			break;
		case L'\t':
			printf("табулар");
			break;
		case L'\0':
			printf("нул");
			break;
		default:
			printf("%lc", CHR(input));
		}
		break;
	default:
		break;
	}
}

#ifdef DEBUG
void PrintEntry(entry *e)
{
	if (e == NULL)
	{
		return;
	}
	PrintEntry(e->left);
	printf("%s: ", e->name);
	Print(e->value);
	PrintEntry(e->right);
}

void PrintEnv(env environment)
{
	printf("%p", (void *) environment);
	env enclosings = environment->enclosing;
	while (enclosings != NULL)
	{
		printf(" <-- %p", (void *) enclosings);
		enclosings = enclosings->enclosing;
	}
	printf("\n");
	PrintEntry(environment->table);
}
#endif
